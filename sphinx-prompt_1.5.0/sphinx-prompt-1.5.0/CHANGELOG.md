# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.5.0](https://github.com/sbrunner/sphinx-prompt/releases/tag/1.5.0) - 2021-08-09

<small>[Compare with 1.4.0](https://github.com/sbrunner/sphinx-prompt/compare/1.4.0...1.5.0)</small>

### Added
- Add pipfile.lock to .prettierignore ([a7517ac](https://github.com/sbrunner/sphinx-prompt/commit/a7517acade826e4f2059c4cd1549390c04ea29b3) by Stéphane Brunner).
- Add typed metadata ([f68d913](https://github.com/sbrunner/sphinx-prompt/commit/f68d9139a99454815639f8a8bca61076bec72ee1) by Stéphane Brunner).
- Add types ([6841094](https://github.com/sbrunner/sphinx-prompt/commit/6841094c8156f1aa41c1482b6159496b143187cd) by Stéphane Brunner).
- Add test for auto ([506c43f](https://github.com/sbrunner/sphinx-prompt/commit/506c43fad6ceafab5a4efdc86dc5ed8a190d8267) by Stéphane Brunner).
- Add some tests ([05178e2](https://github.com/sbrunner/sphinx-prompt/commit/05178e2e1378878849fd777684653861ead6d3d8) by Stéphane Brunner).

### Fixed
- Fix dependabot auto merge ([d3d6d86](https://github.com/sbrunner/sphinx-prompt/commit/d3d6d86cd1ce85a1673f1ee41f5bfc83aae1d187) by Stéphane Brunner).
- Fix dependabot auto-merge ([b838767](https://github.com/sbrunner/sphinx-prompt/commit/b838767593c1c01b39a5c6a0930e75acfcc948ba) by Stéphane Brunner).
- Fix new line in tests ([b1198a0](https://github.com/sbrunner/sphinx-prompt/commit/b1198a074db77eab14af08474b15f7dd128ea46e) by Stéphane Brunner).

### Removed
- Remove strict format ([f66111a](https://github.com/sbrunner/sphinx-prompt/commit/f66111a6a76b00e51c840d700a27153093bb1027) by Stéphane Brunner).


## [1.4.0](https://github.com/sbrunner/sphinx-prompt/releases/tag/1.4.0) - 2021-03-22

<small>[Compare with 1.3.0](https://github.com/sbrunner/sphinx-prompt/compare/1.3.0...1.4.0)</small>

### Added
- Add `batch` and `powershell` languages ([f0dab7f](https://github.com/sbrunner/sphinx-prompt/commit/f0dab7ff6ea80cf1f5e2aceb716ae37d342c9a93) by jack1142).
- Add check, reformat, remove python 2 support ([a64b58e](https://github.com/sbrunner/sphinx-prompt/commit/a64b58e80b719058301a80893d07ee4385611d14) by Stéphane Brunner).

### Fixed
- Fix warning when mixing positional args and options ([5c678a3](https://github.com/sbrunner/sphinx-prompt/commit/5c678a319910914101e4e256f60e806eb948b1a2) by jack1142).
- Fix additional spaces after the prompt when using multiple ([90327a4](https://github.com/sbrunner/sphinx-prompt/commit/90327a436f51e34969def5c3a8caf375b5dfb4f3) by jack1142).
- Fix incorrect line continuation handling for batch and powershell ([fd7156a](https://github.com/sbrunner/sphinx-prompt/commit/fd7156ac04aa9799b4aec116ec6f52ecea72326b) by jack1142).
- Fix config ([1f8d9f7](https://github.com/sbrunner/sphinx-prompt/commit/1f8d9f7262c50b6d93b4866ef2cf14811b05b5ba) by Stéphane Brunner).


## [1.3.0](https://github.com/sbrunner/sphinx-prompt/releases/tag/1.3.0) - 2020-09-09

<small>[Compare with 1.2.0](https://github.com/sbrunner/sphinx-prompt/compare/1.2.0...1.3.0)</small>

### Removed
- Remove newlines from `setup(description=...)` ([d793d0a](https://github.com/sbrunner/sphinx-prompt/commit/d793d0af41312fdebbdced0f368d351042758f9b) by John T. Wodder II).


## [1.2.0](https://github.com/sbrunner/sphinx-prompt/releases/tag/1.2.0) - 2020-05-11

<small>[Compare with 1.1.0](https://github.com/sbrunner/sphinx-prompt/compare/1.1.0...1.2.0)</small>

### Added
- Add ability to read and write parallel ([63690e2](https://github.com/sbrunner/sphinx-prompt/commit/63690e224493090d3635ffb7dc5410bfbf9803a5) by Eric Holscher).


## [1.1.0](https://github.com/sbrunner/sphinx-prompt/releases/tag/1.1.0) - 2019-04-03

<small>[Compare with 1.0.0](https://github.com/sbrunner/sphinx-prompt/compare/1.0.0...1.1.0)</small>

### Added
- Add 3-clause bsd license and include in source distribution. ([49d2c20](https://github.com/sbrunner/sphinx-prompt/commit/49d2c2077577a4e0922a6047d75dd0a6217483f5) by Greg Back). Related issues/PRs: [#17](https://github.com/sbrunner/sphinx-prompt/issues/17)

### Fixed
- Fix avoid unnecessary name repetition in equality comparisions ([b8695d1](https://github.com/sbrunner/sphinx-prompt/commit/b8695d177f07003b58b6c51d2b28d8cef2e54467) by Stéphane Brunner).


## [1.0.0](https://github.com/sbrunner/sphinx-prompt/releases/tag/1.0.0) - 2015-06-07

<small>[Compare with 0.2.1](https://github.com/sbrunner/sphinx-prompt/compare/0.2.1...1.0.0)</small>

### Fixed
- Fix some typos ([fa8cb0f](https://github.com/sbrunner/sphinx-prompt/commit/fa8cb0f5be84af440462eee3c4e793dd7bf78db0) by Jean-Paul Calderone).


## [0.2.1](https://github.com/sbrunner/sphinx-prompt/releases/tag/0.2.1) - 2013-12-20

<small>[Compare with 0.2](https://github.com/sbrunner/sphinx-prompt/compare/0.2...0.2.1)</small>

### Fixed
- Fix auto modifier ([cc047e4](https://github.com/sbrunner/sphinx-prompt/commit/cc047e4f01fbd0f74431d247459fde6d98e5fb12) by Stéphane Brunner).


## [0.2](https://github.com/sbrunner/sphinx-prompt/releases/tag/0.2) - 2013-10-29

<small>[Compare with 0.1](https://github.com/sbrunner/sphinx-prompt/compare/0.1...0.2)</small>

### Added
- Add pigments ([a50aee7](https://github.com/sbrunner/sphinx-prompt/commit/a50aee76f544d460628a2decb6d52a18b6029e80) by Stéphane Brunner).
- Add type auto (as default) ([8cd5e53](https://github.com/sbrunner/sphinx-prompt/commit/8cd5e53595e6a196c87491df1635e5a9cd2649ce) by Stéphane Brunner).

### Changed
- Change default values ([c4cbd0a](https://github.com/sbrunner/sphinx-prompt/commit/c4cbd0a371ed0ea5f0e3cb3993c675dd1fd2a03d) by Stéphane Brunner).

### Fixed
- Fix language ([6208472](https://github.com/sbrunner/sphinx-prompt/commit/62084726f5d62057344d0b7af0599d582cbab796) by Stéphane Brunner).

### Removed
- Remove unexpected linebreak ([2aed0ea](https://github.com/sbrunner/sphinx-prompt/commit/2aed0eaadee180c6774d0748da565b0e1ac5350f) by Stéphane Brunner).


## [0.1](https://github.com/sbrunner/sphinx-prompt/releases/tag/0.1) - 2013-10-13

<small>[Compare with first commit](https://github.com/sbrunner/sphinx-prompt/compare/2016925b7d951100b03bdedb2312b75f2f9caea2...0.1)</small>


