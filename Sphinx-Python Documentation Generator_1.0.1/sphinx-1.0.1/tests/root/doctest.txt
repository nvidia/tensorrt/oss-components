Testing the doctest extension
=============================

Simple doctest blocks
---------------------

>>> 1+1
2
>>> 1/0
Traceback (most recent call last):
  ...
ZeroDivisionError: integer division or modulo by zero


Special directives
------------------

* doctest

  .. doctest::

     >>> 1+1
     2
     >>> 1/0
     Traceback (most recent call last):
       ...
     ZeroDivisionError: integer division or modulo by zero

* testcode/testoutput

  .. testcode::

     print 1+1

  .. testoutput::

     2

  .. testcode::

     1/0

  .. testoutput::

     Traceback (most recent call last):
       ...
     ZeroDivisionError: integer division or modulo by zero

* testsetup

  .. testsetup:: *

     from math import floor

  .. doctest::

     >>> floor(1.2)
     1.0

  .. testcode::

     print floor(1.2)

  .. testoutput::

     1.0

  >>> floor(1.2)
  1.0

* options for testcode/testoutput blocks

  .. testcode::
     :hide:

     print 'Output         text.'

  .. testoutput::
     :hide:
     :options: +NORMALIZE_WHITESPACE

     Output text.

* grouping

  .. testsetup:: group1

     from math import ceil

  ``ceil`` is now known in "group1", but not in others.

  .. doctest:: group1

     >>> ceil(0.8)
     1.0

  .. doctest:: group2

     >>> ceil(0.8)
     Traceback (most recent call last):
       ...
     NameError: name 'ceil' is not defined

  Interleaving testcode/testoutput:

  .. testcode:: group1

     print ceil(0.8)

  .. testcode:: group2

     print floor(0.8)

  .. testoutput:: group1

     1.0

  .. testoutput:: group2

     0.0
